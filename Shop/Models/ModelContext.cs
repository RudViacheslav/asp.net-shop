﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Shop.Models
{
    public class ModelContext : DbContext
    {
        static ModelContext()
        {
            Database.SetInitializer(new ModelContextInitializer());

        }
        public ModelContext() : base("DefaultConnection")
        {
            Database.Initialize(true);
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
    }
}