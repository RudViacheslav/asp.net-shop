﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.Models
{
    public class Product
    {
        public int ID { get; set; }
        public string Name { get;set; }
        public int Cost { get; set; }

        public Product()
        {
            OrderItems = new HashSet<OrderItem>();
        }

        public virtual ICollection<OrderItem> OrderItems { get; set; }
    }
}