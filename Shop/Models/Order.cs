﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.Models
{
    public class Order
    {
        public int ID { get; set; }
        public string CustomerID { get; set; }

        public Order()
        {
            OrderItems = new HashSet<OrderItem>();
        }

        public virtual Customer Customer { get; set; }

        public virtual ICollection<OrderItem> OrderItems { get; set; }
    }
}