﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Shop.Models
{
    public class ModelContextInitializer : DropCreateDatabaseAlways<ModelContext>
    {
        protected override void Seed(ModelContext context)
        {
            context.Customers.Add(new Customer { Name = "Сергей" });
            context.Customers.Add(new Customer { Name = "Ирина" });
            context.Customers.Add(new Customer { Name = "Николай" });
            context.Customers.Add(new Customer { Name = "Юлия" });
            context.Customers.Add(new Customer { Name = "Константин" });
            context.Customers.Add(new Customer { Name = "Евгений" });

            context.Products.Add(new Product { Name = "Samsung RB37J5000SA", Cost = 14_049 });
            context.Products.Add(new Product { Name = "Sharp SJ-X640-HS3", Cost = 16_699 });
            context.Products.Add(new Product { Name = "Bosch KGN39XL35", Cost = 12_861 });
            context.Products.Add(new Product { Name = "Beko RCNA355E21W", Cost = 10_881 });
            context.Products.Add(new Product { Name = "Samsung RB30J3000WW", Cost = 3_801 });
            context.Products.Add(new Product { Name = "Delfa DF-85", Cost = 11_820 });
            context.Products.Add(new Product { Name = "Samsung RB30J3000SA", Cost = 7_138 });
            context.Products.Add(new Product { Name = "Sharp SJ-B1239M4W", Cost = 4_572 });
            context.Products.Add(new Product { Name = "Whirlpool ART 9811/A++ SF", Cost = 17_431 });
            context.Products.Add(new Product { Name = "Whirlpool SP40 801 EU", Cost = 25_398 });
            context.Products.Add(new Product { Name = "Samsung RB29FSRNDSA", Cost = 12_706 });
            context.Products.Add(new Product { Name = "ATLANT МХМ 2835-9", Cost = 6_861 });
            context.Products.Add(new Product { Name = "Bosch KGN39ML3", Cost = 14_206 });
            context.Products.Add(new Product { Name = "Elenberg MRF-221-O", Cost = 4_881 });
            context.SaveChanges();
        }
    }
}